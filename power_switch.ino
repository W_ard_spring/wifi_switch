#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266TrueRandom.h>
#include <EEPROM.h>

const char* device_name = "switch";   // This device is AC 220V power switch.
int upload_interval = 1000;       // Uploading switch state in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

const char* power_on_set_suffix = "/power_on/set";
const char* power_on_state_suffix = "/power_on/state";

int PIN_PowerOn = 4; // Use D2, GPIO4  (setting pin to open the relay)
int PIN_PowerOn_Led = 5;    // Use D1, GPIO5  (led pin to show relay state)

// Global variables

WiFiClient espClient;
PubSubClient client(espClient);

byte uuidNumber[16]; // UUIDs in binary form are 16 bytes long
String uuid_buf;
char uuid_name[37];  //UUID in char arry type

long lastMsg = 0;
const char* power_on_state = "OFF";

char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {
  pinMode(PIN_PowerOn, OUTPUT);
  digitalWrite(PIN_PowerOn, LOW);
  pinMode(PIN_PowerOn_Led, OUTPUT);
  digitalWrite(PIN_PowerOn_Led, LOW);

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  EEPROM.begin(512);  // store values of relay state and brightness level setting

  ESP8266TrueRandom.uuid(uuidNumber);
  Serial.print("The UUID number is ");
  printUuid(uuidNumber);
  Serial.println();
  uuid_buf = ESP8266TrueRandom.uuidToString(uuidNumber);
  uuid_buf.toCharArray(uuid_name, 37);
  Serial.println(uuid_name);
}

void setup_wifi() {

  int attempt = 0;

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");

  // Check topic of power on set and open led
  set_sub_topic(power_on_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {
      power_on_state = "ON";
      Serial.println("RELAY is turning ON state...");
      digitalWrite(PIN_PowerOn_Led, HIGH);
      digitalWrite(PIN_PowerOn, HIGH);
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {
      power_on_state = "OFF";
      Serial.println("RELAY is turning OFF state...");
      digitalWrite(PIN_PowerOn_Led, LOW);
      digitalWrite(PIN_PowerOn, LOW);
    }
  }
  EEPROM_Write();
}

void printHex(byte number) {
  int topDigit = number >> 4;
  int bottomDigit = number & 0x0f;
  // Print high hex digit
  Serial.print( "0123456789ABCDEF"[topDigit] );
  // Low hex digit
  Serial.print( "0123456789ABCDEF"[bottomDigit] );
}

void printUuid(byte* uuidNumber) {
  int i;
  for (i = 0; i < 16; i++) {
    if (i == 4) Serial.print("-");
    if (i == 6) Serial.print("-");
    if (i == 8) Serial.print("-");
    if (i == 10) Serial.print("-");
    printHex(uuidNumber[i]);
  }
}

void reconnect() {
  // Loop until we're reconnected
  int attempt = 0;

  while (!client.connected()) {

    if (attempt < 3)
      attempt ++;
    else
      ESP.restart();

    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(uuid_name)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(power_on_set_suffix);
      client.subscribe(buf_sub_topic);
      client.subscribe("common");
    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void EEPROM_Write(void) {

  byte val_state;
  
  if (power_on_state == "ON") {
    val_state = 1;
  }
  else if (power_on_state == "OFF") {
    val_state = 0;
  }
  
  EEPROM.write(0, val_state);
  EEPROM.commit();
  
  Serial.println("*********************************");
  Serial.print("val_state: ");
  Serial.println(val_state);
  Serial.println("*********************************");
}

void EEPROM_Read(void) {

  byte val_state;  
  val_state = EEPROM.read(0);

  if (val_state == 1) {
    power_on_state = "ON";
    digitalWrite(PIN_PowerOn_Led, HIGH);
    digitalWrite(PIN_PowerOn, HIGH);
  }
  else if (val_state == 0) {
    power_on_state = "OFF";
    digitalWrite(PIN_PowerOn_Led, LOW);
    digitalWrite(PIN_PowerOn, LOW);
  }  

  Serial.println("************************************************");
  Serial.print("val_state: ");
  Serial.println(val_state);
  Serial.print("State of power is ");
  Serial.println(power_on_state);
  Serial.println("************************************************");
}

void loop() {

  if (!client.connected()) {
    reconnect();
    EEPROM_Read();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > upload_interval) {
    lastMsg = now;

    Serial.print("Power state:  ");
    Serial.println(power_on_state);

    // publish state of power on
    set_pub_topic(power_on_state_suffix);
    client.publish(buf_pub_topic, power_on_state);
  }
  else {
    delay(400);   // Loop function takes about 300ms, so 400 ms is enough.
  }
}

void set_pub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}
